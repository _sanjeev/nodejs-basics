// GLOBALS - NO WINDOW !!!!

// __dirname - path of the current directory
// __filename - file name
// require - function to use module (COMMON JS)
// module - info about current module(file)
// process - info about env where the program is being executed

const directoryName = __dirname;
console.log("🚀 ~ file: app.js:10 ~ directoryName", directoryName);
const filename = __filename;
console.log("🚀 ~ file: app.js:12 ~ filename", filename);

let clear = false;
let myInterval = setInterval(() => {
    clear = true;
    console.log("interval run every second");
    if (clear) clearInterval(myInterval);
}, 1000);
