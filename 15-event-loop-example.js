const http = require("http");

const server = http.createServer((req, res) => {
    if (req.url === "/") {
        return res.end("<h1>Welcome to my page</h1>");
    }

    if (req.url === "/about") {
        //BLOCKING CODE
        for (let i = 0; i < 10000; i++) {
            for (let j = 0; j < 20000000; j++) {
                console.log(`${i} , ${j}`);
            }
        }
        return res.end("<h1>Welcome to about page</h1>");
    }

    return res.end("<h1>404: Not found!</h1>");
});

server.listen(5000, (err) => {
    if (err) {
        console.log("Error : ", err);
        return;
    }
    console.log("Server is listening on port : 5000");
});
