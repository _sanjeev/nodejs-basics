const { readFile, writeFile } = require("fs");

readFile("./content/file1.txt", "utf-8", (err, data) => {
    if (err) {
        console.log("Error : ", err);
        return;
    }
    const firstFileData = data;
    readFile("./content/file2.txt", "utf-8", (err, data) => {
        if (err) {
            console.log("Error : ", err);
            return;
        }
        const secondData = data;
        writeFile("./content/result1.txt", `Here is the content : ${firstFileData} ${secondData}`, (err) => {
            if (err) {
                console.log("Error : ", err);
                return;
            }
        });
    });
});
