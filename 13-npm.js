// npm - global command, comes with Node
// npm --version

// local dependency - use it only in this particular PromiseRejectionEvent
// npm i <packageName>

// global dependency - use it in my PromiseRejectionEvent

// npm i -g <packageName>

// package.json - manifest file (stores important info about project/package)
// manual approach (create package.json in the root, create properties ets)
// npm init (step by step, press enter to skip)
// npm init -y (everything default)
const _ = require("lodash");
const items = [1, [2, [3, [4, 5], [6, [7]]]]];
const result = _.flatMapDeep(items);
console.log("🚀 ~ file: 13-npm.js:18 ~ result", result);
