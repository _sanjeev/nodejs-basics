const http = require("http");

const server = http.createServer((req, res) => {
    if (req.url === "/") {
        return res.end("Welcome to home page");
    }
    if (req.url === "/about") {
        return res.end("Welcome to about page");
    }
    return res.end(`
    <h1>404 : Not found!</h1>
    <a href='/'>Go to home</a>`);
});

server.listen(5000, (err) => {
    if (err) {
        console.log("Error : ", err);
        return;
    }
    console.log("Server is listening on port ", 5000);
});
