// Modules

//COMMONJS - every file in node is a module by default
// Modules - Encapsulated code (only share minimum)
const names = require("./4-names");
const sayHi = require("./5-utils");
const { singlePerson, items } = require("./6-alternative-modules");
require("./7-mind-grended");
sayHi("sujan");
sayHi(names.john);
sayHi(names.peter);
