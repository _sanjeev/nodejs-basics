const os = require("os");

// info about user info
const user = os.userInfo();
console.log("🚀 ~ file: app.js:5 ~ user", user);

//info about computer has running in second
const uptime = os.uptime();
console.log(`System uptime is ${uptime} sec`);

//info about current operating system
const currentOS = {
    name: os.type(),
    release: os.release(),
    totalMem: os.totalmem(),
    freeMem: os.freemem(),
};
console.log("🚀 ~ file: app.js:17 ~ currentOS", currentOS);
