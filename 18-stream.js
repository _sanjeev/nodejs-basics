const { createReadStream } = require("fs");

const stream = createReadStream("./content/file1.txt", { highWaterMark: 9000000, encoding: "utf-8" });

stream.on("data", (result) => {
    console.log(result);
});

stream.on("error", (err) => {
    console.log("error", err);
});
