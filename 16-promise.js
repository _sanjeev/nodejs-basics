const { readFile } = require("fs");

const getText = (pathName) => {
    return new Promise((resolve, reject) => {
        readFile(pathName, "utf8", (err, data) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(data);
        });
    });
};

getText("./content/file1.txt")
    .then((response) => {
        console.log("🚀 ~ file: app.js:16 ~ getText ~ response", response);
    })
    .catch((error) => {
        console.log("🚀 ~ file: app.js:18 ~ getText ~ error", error);
    });
