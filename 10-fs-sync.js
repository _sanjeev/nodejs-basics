const { writeFileSync, readFileSync } = require("fs");

const first = readFileSync("./content/file1.txt", "utf-8");
console.log(first);

const second = readFileSync("./content/file2.txt", "utf-8");
console.log(second);

writeFileSync("./content/result.txt", `Here is the result ${first}, ${second}`, {
    flag: "a",
});
