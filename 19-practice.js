//Event emitter example

const EventEmitter = require("events");

const eventEmitter = new EventEmitter();

eventEmitter.on("countAPI", () => {
    console.log("Event called");
});

eventEmitter.emit("countAPI");

//Streams in nodejs

const fs = require("fs");
const readStream = fs.createReadStream("./content/big.txt", { highWaterMark: 100 });
// for (let i = 0; i < 10000; i++) {
//     fs.writeFile("./content/big.txt", `hello world ${i} \n`, { flag: "a" }, (err) => {
//         if (err) {
//             console.log("err");
//             return;
//         }
//         console.log("write data successfully");
//     });
// },

readStream.on("data", (buffer) => {
    console.log("DATA", buffer);
});

readStream.on("end", () => {
    console.log("stream ended");
});
