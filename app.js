const http = require("http");
const fs = require("fs");

const server = http.createServer((req, res) => {
    const createReadFileStream = fs.createReadStream("./content/big.txt", { highWaterMark: 100 });
    // createReadFileStream.on("data", (chunkData) => {
    //     return res.write(chunkData);
    // });
    // createReadFileStream.on("end", () => {
    //     return res.end();
    // });
    // createReadFileStream.on("error", (err) => {
    //     console.log("err", err);
    //     return res.end(err.toString());
    // });

    createReadFileStream.pipe(res);
});

// server.on("request", (req, res) => {
//     const createReadFileStream = fs.createReadStream("./content/big.txt", { highWaterMark: 100 });
//     createReadFileStream.on("data", (chunkData) => {
//         return res.write(chunkData);
//     });
//     createReadFileStream.on("end", () => {
//         return res.end();
//     });
// });

server.listen(5000, (err) => {
    if (err) {
        console.log("Error : ", err);
        return;
    }
    console.log("Server is listening on port : 5000");
});
